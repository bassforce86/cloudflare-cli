package measurements

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"time"
)

var uploadMeasurements = map[string]*Measurement{
	"100kB": {
		Type:  "",
		Bytes: 101000,
		Count: 10,
	},
	"1MB": {
		Type:  "up",
		Count: 8,
		Bytes: 1001000,
	},
	"10kB": {
		Type:  "up",
		Count: 10,
		Bytes: 11000,
	},
}

func Upload(ctx context.Context) error {
	results := map[string][]*Measurement{}

	start := time.Now()

	for k, v := range uploadMeasurements {

		for i := 0; i < int(v.Count); i++ {
			slog.DebugContext(ctx, "Running upload test", slog.Group("test", slog.String("type", k), slog.Int("iteration", i)))

			ittr := *v
			if err := speedRequest(ctx, &ittr); err != nil {
				return fmt.Errorf("upload request failed: %w", err)
			}
			results[k] = append(results[k], &ittr)
		}
	}

	elapsed := time.Since(start)

	total := float64(0)

	for k, v := range results {
		bps, err := speed(ctx, v)
		if err != nil {
			return fmt.Errorf("unable to calculate speed: %w", err)
		}
		slog.DebugContext(ctx, k, slog.String("bps", fmt.Sprintf("%.2f", bps)))
		total = bps + total
	}

	mbps := total / 1000000
	MBs := total / 12500000

	slog.DebugContext(ctx, "upload speed",  slog.String("mbps", fmt.Sprintf("%.2f", (mbps/elapsed.Seconds()))), slog.String("MBs", fmt.Sprintf("%.2F", MBs/elapsed.Seconds())))

	fmt.Fprintf(os.Stdout, "Upload speed: %.2f mpbs\n", (mbps/elapsed.Seconds()))

	return nil

}

