package measurements

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
)

func Location(ctx context.Context) error {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, BaseURL+"/locations", nil)
	if err != nil {
		return fmt.Errorf("unable to request locations: %w", err)
	}

	// start := time.Now()
	resp, err := client.Do(req)
	// elapsed := time.Since(start)

	if err != nil {
		return fmt.Errorf("unable to fetch response: %w", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return errors.New("request was unsuccessful")
	}

	body, err := io.ReadAll(resp.Body)

	var data map[string]any
	json.Unmarshal(body, &data)

	for k, v := range data {
		slog.DebugContext(ctx, k, slog.Any("value", v))
	}

	return nil
}
