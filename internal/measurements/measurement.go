package measurements

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"time"
)

const BaseURL = "https://speed.cloudflare.com"

type Measurement struct {
	Type    string
	Bytes   int64
	Count   int64
	Elapsed time.Duration
}

var client = http.Client{
	Timeout: 2 * time.Second,
}

func speed(ctx context.Context, measurements []*Measurement) (float64, error) {
	total := float64(0)
	for _, m := range measurements {
		// convert bytes to bits
		bits := float64(m.Bytes * 8)
		elapsed := m.Elapsed.Seconds()
		// calc bits per second for measurement
		speed := bits / elapsed
		// sum all bits per second for all measurements
		total = total + speed
	}
	return total / float64(len(measurements)), nil

}

func speedRequest(ctx context.Context, measurement *Measurement) error {
	url := fmt.Sprintf("%s/__down?bytes=%d", BaseURL, measurement.Bytes)
	method := http.MethodGet
	var body io.Reader

	if measurement.Type == "up" {
		url = fmt.Sprintf("%s/__up", BaseURL)
		method = http.MethodPost
		body = bytes.NewBuffer(make([]byte,0, measurement.Bytes))
	}

	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return fmt.Errorf("request building failed: %w", err)
	}

	start := time.Now()
	resp, err := client.Do(req)

	measurement.Elapsed = time.Since(start)

	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("invalid statuscode returned: %d", resp.StatusCode)
	}
	slog.DebugContext(ctx, "results", slog.Group("response",
		slog.String("status", resp.Status),
		slog.Duration("duration", measurement.Elapsed),
	))

	return nil
}
