package measurements

import (
	"context"
	"fmt"
	"log/slog"
	"os"
	"time"
)

var dlmeasurements = map[string]*Measurement{
	"100kB": {
		Type:  "down",
		Bytes: 101000,
		Count: 10,
	},
	"1MB": {
		Type:  "down",
		Count: 8,
		Bytes: 1001000,
	},
	"10MB": {
		Type:  "down",
		Count: 6,
		Bytes: 10001000,
	},
	"25MB": {
		Type:  "down",
		Count: 4,
		Bytes: 25001000,
	},
	"100MB": {
		Type:  "down",
		Count: 1,
		Bytes: 100001000,
	},
}

func Download(ctx context.Context) error {
	results := map[string][]*Measurement{}

	start := time.Now()

	for k, v := range dlmeasurements {

		for i := 0; i < int(v.Count); i++ {
			slog.DebugContext(ctx, "Running download test", slog.Group("test", slog.String("type", k), slog.Int("iteration", i)))

			ittr := *v
			if err := speedRequest(ctx, &ittr); err != nil {
				return fmt.Errorf("download request failed: %w", err)
			}
			results[k] = append(results[k], &ittr)
		}
	}

	elapsed := time.Since(start)

	total := float64(0)

	for k, v := range results {
		bps, err := speed(ctx, v)
		if err != nil {
			return fmt.Errorf("unable to calculate speed: %w", err)
		}
		slog.DebugContext(ctx, k, slog.String("bps", fmt.Sprintf("%.2f", bps)))

		total = bps + total

	}

	mbps := total / 10000000
	MBs := total / 12500000

	slog.DebugContext(ctx, "download speed", slog.String("mbps", fmt.Sprintf("%.2f", (mbps/elapsed.Seconds()))), slog.String("MBs", fmt.Sprintf("%.2F", MBs/elapsed.Seconds())))

	fmt.Fprintf(os.Stdout, "Download speed: %.2f mbps\n", (mbps/elapsed.Seconds()))

	return nil

}

