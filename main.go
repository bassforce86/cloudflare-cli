package main

import (
	"context"
	"flag"
	"log/slog"
	"os"

	"gitlab.com/bassforce86/cloudflare-cli/internal/measurements"
	"golang.org/x/sync/errgroup"
)

var debug bool

func main() {
	flag.BoolVar(&debug, "debug", false, "enable debug logging")
	flag.Parse()

	if debug {
		slog.SetLogLoggerLevel(slog.LevelDebug)
	}

	g, ctx := errgroup.WithContext(context.Background())

	g.Go(func() error { return measurements.Download(ctx) })
	g.Go(func() error { return measurements.Upload(ctx) })

	if err := g.Wait(); err != nil {
		slog.ErrorContext(ctx, "speed test failed", slog.Any("error", err))
		os.Exit(1)
	}

	os.Exit(0)
}
